# variables-and-secrets

Die Einstellungen für CI Variablen und Secrets finden sind im Projekt unter `Settings` -> `CI / CD` -> `Variables`.

Weitere Infromationen zu `variables` finden sich hier:
- [`variables` keyword](https://docs.gitlab.com/ee/ci/yaml/#variables)
- [Vordefinierte Environment-Variablen](https://docs.gitlab.com/ee/ci/variables/README.html#predefined-environment-variables)
- [Priorität/Präzedenz von Environment-Variablen](https://docs.gitlab.com/ee/ci/variables/README.html#priority-of-environment-variables)
